FROM python:3.7.9
RUN /usr/local/bin/python -m pip install --upgrade pip
WORKDIR /intent-entities
COPY . /intent-entities
RUN pip3 --no-cache-dir install -r requirements.txt
RUN rasa train nlu -c nlp_engine/config.yml --nlu nlp_engine/data/nlu.yml --out nlp_engine/models
CMD ["uvicorn", "app:app", "--host", "0.0.0.0"]
