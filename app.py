import logging
import os
import subprocess
import threading

import yaml
from fastapi import FastAPI, HTTPException

from nlp_engine.processor import process_message
from utils.constant import ROOT_DIR, CURRENT_TIME
from utils.data_converter import bs64_to_file
from utils.object_models import UserMessage, UserFile, UserCfg, UserCfgFile

# logging configuration
logging.basicConfig(filename='log_app.log',
                    format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.DEBUG)

app = FastAPI()


@app.post("/parse_message")  # , response_model=Response
def parse_message(item: UserMessage):
    """
     Endpoint to parse a given message.
     "model_name" parameter is optional, if not given the model would go  with the default model
    """
    return process_message(item.message, item.model_name)


@app.post("/train_model")
def retrain_model(item: UserFile):
    """
     Endpoint to train a new model, given:
     - "nlu_file" nlu file in (base64) format, to use the default nlu file let this parameter empty.
     - "cfg_name" name of config file to use with extension .yml (you can get list of available config files using
     "/cfgs" endpoint), to use the default config file let this parameter empty.
     - "model_name" name to use to save the model after training
    """
    nlu_file = item.nlu_file
    cfg_file = item.cfg_name
    model_name = item.model_name
    path_nlu = str(ROOT_DIR / "nlp_engine/data/nlu.yml")
    if nlu_file:
        path = ROOT_DIR / "nlp_engine/data/nlu/"
        name = "nlu_" + str(CURRENT_TIME)
        for i in [" ", "-", ":", "."]:
            name = name.replace(i, "_")
        path_nlu = bs64_to_file(nlu_file, name + ".yml", path)
    path_config = str(ROOT_DIR / "nlp_engine/config.yml")
    if cfg_file and cfg_file != "config.yml":
        path_config = ROOT_DIR / "nlp_engine/data/cfg/" / cfg_file
    path_model = str(ROOT_DIR / "nlp_engine/models")
    cmd = f"rasa train nlu -c {path_config} --nlu {path_nlu} --out {path_model} --fixed-model-name {model_name}"
    # if item.retrain:
    #     cmd += " --persist-nlu-data"
    th = threading.Thread(name='Cmd', target=subprocess.run, args=[cmd], kwargs={'shell': True})
    th.start()
    return {'model_name': model_name}


@app.post("/upload_cfg")
def upload_cfg(item: UserCfgFile):
    """
     Endpoint to upload a new config file, given:
     - "cfg_file" config file in (base64) format. (you can get list of available config files using
     "/cfgs" endpoint)
    """
    cfg_file = item.cfg_file
    path = ROOT_DIR / "nlp_engine/data/cfg/"
    name = "cfg_" + str(CURRENT_TIME)
    for i in [" ", "-", ":", "."]:
        name = name.replace(i, "_")
    bs64_to_file(cfg_file, name + ".yml", path)
    return {"cfg_name": name + ".yml"}


@app.get("/models")
def list_models():
    """
     Endpoint to list all available models.
    """
    path_model = str(ROOT_DIR / "nlp_engine/models")
    res = {"models": []}
    for folder, under_folders, files in os.walk(path_model):
        for file in files:
            if file.split(".")[-1] == "gz":
                res["models"].append(file)
    return res


@app.get("/cfgs")
def list_cfgs():
    """
     Endpoint to list available config files.
    """
    path_cfg = str(ROOT_DIR / "nlp_engine/data/cfg")
    res = {"cfgs": ["config.yml"]}
    for folder, under_folders, files in os.walk(path_cfg):
        for file in files:
            if file.split(".")[-1] == "yml":
                res["cfgs"].append(file)
    return res


@app.post("/cfgs/read")
def read_cfgs(item: UserCfg):
    """
     Endpoint to read a config file content, given:
     - "cfg_name" name of config file to use with extension .yml (you can get list of available config files using
     "/cfgs" endpoint)
    """
    if item.cfg_name.split(".")[-1] != "yml":
        raise HTTPException(status_code=400, detail="Invalid yml file name")
    path_cfg = str(ROOT_DIR / "nlp_engine/data/cfg" / item.cfg_name)
    if item.cfg_name == "config.yml":
        path_cfg = str(ROOT_DIR / "nlp_engine" / item.cfg_name)
    with open(path_cfg, 'r') as yaml_in:
        res = yaml.safe_load(yaml_in)
    return res
