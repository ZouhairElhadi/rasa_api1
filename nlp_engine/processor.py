import asyncio
import glob
import os
import subprocess

from rasa.core.agent import Agent
from rasa.shared.utils.cli import print_success

from utils.constant import ROOT_DIR

list_of_files = glob.glob('nlp_engine/models/*.tar.gz')
if not list_of_files:
    path_config = str(ROOT_DIR / "nlp_engine/config.yml")
    path_nlu = str(ROOT_DIR / "nlp_engine/data/nlu.yml")
    path_model = str(ROOT_DIR / "nlp_engine/models")
    cmd = f"rasa train nlu -c {path_config} --nlu {path_nlu} --out {path_model}"
    subprocess.run(cmd, capture_output=True, shell=True)
    list_of_files = glob.glob('nlp_engine/models/*.tar.gz')
model_path = max(list_of_files, key=os.path.getctime)
print(model_path)

agent = Agent.load(model_path)
print_success(f"NLU model loaded: {model_path}")

def process_message(message, model_path=None):
    if model_path: 
        agent_tmp = Agent.load(model_path)
        result = asyncio.run(agent_tmp.parse_message(message))
    else:
        result = asyncio.run(agent.parse_message(message))
    result.pop('text_tokens', None)
    result.pop('text', None)
    result.pop('response_selector', None)
    return result
