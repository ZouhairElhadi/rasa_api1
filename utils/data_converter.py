import base64
import os


def bs64_to_file(file: str, name, path):
    if not os.path.isdir(path):
        os.makedirs(path)
    name = path / name
    file = base64.b64decode(str(file))
    f = open(name, 'wb')
    f.write(file)
    f.close()
    return str(name)
