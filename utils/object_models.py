from typing import List, Optional, Union
from pydantic import BaseModel


class IntentObject(BaseModel):
    name: str
    confidence: float


class EntitiesObject(BaseModel):
    entity: str
    start: int
    end: int
    confidence_entity: float
    value: str
    extractor: str


class UserMessage(BaseModel):
    message: str
    model_name: Optional[str]


class UserFile(BaseModel):
    nlu_file: str
    cfg_name: str
    model_name: str
    #retrain: bool


class UserCfgFile(BaseModel):
    cfg_file: str


class UserCfg(BaseModel):
    cfg_name: str


class Response(BaseModel):
    intent: IntentObject
    entities: EntitiesObject
    intent_ranking: List[IntentObject]


class ResponseModels(BaseModel):
    models: List[str]
