from datetime import datetime
from pathlib import Path

ROOT_DIR = Path(__file__).absolute().parent.parent
CURRENT_TIME = datetime.now()
